import React, { useEffect, useState } from "react";
import axios from "../axios";
import requests from "../requests";
import "./banner.css";

export default function Banner({ handleTrailerClick }) {
  const [movie, setMovie] = useState("");

  useEffect(() => {
    async function fetchData() {
      try {
        const response = await axios.get(requests.originals);
        setMovie(
          response.data.results[
            Math.floor(Math.random() * response.data.results.length - 1)
          ]
        );
        return response;
      } catch (e) {
        console.log(requests.originals);
      }
    }

    fetchData();
  }, []);

  function truncate(str, n) {
    return str?.length > n ? str.substr(0, n - 1) + "..." : str;
  }

  return (
    <header
      className="banner"
      style={{
        backgroundSize: "cover",
        backgroundImage: `url(https://image.tmdb.org/t/p/original/${movie.backdrop_path})`,
        backgroundPosition: "center center",
      }}
    >
      <div className="banner__contents">
        <h1 className="banner__title">
          {movie?.title || movie?.name || movie?.original_name}
        </h1>
        {/*2 button*/}
        <div className="banner__buttons">
          <button
            className="banner__button"
            onClick={() => handleTrailerClick(movie)}
          >
            Play
          </button>
          <button className="banner__button">My List</button>

          <h1 className="banner__description">
            {truncate(movie?.overview, 150)}
          </h1>
        </div>
        {/*description*/}
      </div>

      <div className="banner--fadeBottom"></div>
    </header>
  );
}
