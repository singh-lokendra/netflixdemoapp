import React from "react";
import "./App.css";
import Row from "./app/row/Row";
import requests from "./app/requests";
import Banner from "./app/banner/Banner";
import Nav from "./app/Nav/Nav";

function App() {
  return (
    <div className="App">
      <Nav />
      <Banner />

      <Row title="Netlix Originals" fetchUrl={requests.originals} isLarge />
      <Row title="Trending Now" fetchUrl={requests.trending} />
      <Row title="Action Movies" fetchUrl={requests.actionMovies}/>
      <Row title="Comedy Movies" fetchUrl={requests.comedyMovies}/>
      <Row title="Documentaries" fetchUrl={requests.documentaries}/>
      <Row title="Horror Movies" fetchUrl={requests.horrorMovies}/>
      <Row title="Romance Movies" fetchUrl={requests.romanceMovies}/>
    </div>
  );
}

export default App;
